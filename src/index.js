require('./ws')
require('./db/mongoose')
const express = require('express')
const path = require('path')
const Message = require('./models/messages')

const app = express()

app.set('view engine', 'hbs')
app.use(express.static(path.join(__dirname, '../public')))

app.get('/', (req, res) => {
    res.render('index')
})

app.listen(3000)