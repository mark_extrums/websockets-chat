const WebSocket = require('ws')
const uuid = require('uuid')
const Message = require('./models/messages')

const wss = new WebSocket.Server({
    port: '8080'
})

const connections = []

wss.on('connection', async ws => {
    connections.push(ws)

    ws.on('message', async (data) => {
        data = JSON.parse(data)

        switch (data.type) {
            case 'Connect':
                if (!data.user) {
                    ws.send(JSON.stringify({ type: 'New user', user: uuid.v4() }))
                }

                const messages = await Message.find()

                ws.send(JSON.stringify({ type: 'Load messages', messages }))

                break
            case 'New message':
                const { author, msg, userId } = data

                const message = await new Message({ msg, author, userId }).save()

                connections.forEach((el) => {
                    el.send(JSON.stringify({ type: 'New message', message }))
                })
                break
        } 
    })
})    

