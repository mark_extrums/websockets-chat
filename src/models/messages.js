const mongoose = require('mongoose')

const MessageSchema = new mongoose.Schema({
    msg: String,
    author: String,
    userId: String
})

module.exports = mongoose.model('message', MessageSchema)