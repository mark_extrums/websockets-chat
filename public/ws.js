const ws = new WebSocket('ws://localhost:8080')

const generateMsg = (author, msg, userId) => {
    const currentId = localStorage.getItem('user')

    if (currentId === userId) {
        return `<div class="message-block">
                    <div class="message my-message">
                        <div>${msg}</div>
                    </div>
                </div>`
    }

    return `<div>
                <div class="message">
                    <h6>${author}</h6>
                    <div>${msg}</div>
                </div>
            </div>`
}

const onLoadMessages = (msgs) => {
    const chat = document.getElementById('chat')

    msgs.forEach((msg) => {
        chat.innerHTML += generateMsg(msg.author, msg.msg, msg.userId)
    })

    chat.scrollTop = chat.scrollHeight
}

const onLoadMessage = (msg) => {
    const chat = document.getElementById('chat')

    chat.innerHTML += generateMsg(msg.author, msg.msg, msg.userId)

    chat.scrollTop = chat.scrollHeight
}

ws.addEventListener('open', () => {
    ws.send(JSON.stringify({ type: 'Connect', user: localStorage.getItem('user') }))
})

ws.addEventListener('message', (msg) => {
    const data = JSON.parse(msg.data)

    switch (data.type) {
        case 'Load messages':
            onLoadMessages(data.messages)
            break
        case 'New message':
            onLoadMessage(data.message)
            break
        case 'New user':
            localStorage.setItem('user', data.user)
    }
})

document.getElementById('send-btn').addEventListener('click', () => {
    const msg = document.getElementById('message')
    const author = document.getElementById('author')
    const error = document.getElementById('error')

    if (author.value === '') {
        return error.classList.remove('hide')
    }

    ws.send(JSON.stringify({ author: author.value, msg: msg.value, type: 'New message', userId: localStorage.getItem('user') }))
    msg.value = ''
    error.classList.add('hide')
})

